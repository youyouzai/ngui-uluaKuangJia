﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LayerTools))]
public class LayerToolsEditor : Editor {

    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        LayerTools tool = target as LayerTools;

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("sortingOrder");
        tool.sortingOrder = EditorGUILayout.IntField(tool.sortingOrder);
        EditorGUILayout.EndHorizontal();
    }
}
