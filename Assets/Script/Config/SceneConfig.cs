﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneConfig
{
    public static string Loading = "Loading";
    public static string Init = "Init";
    public static string Fight = "Fight";
}
