﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacadeConfig
{
    public static string AssetManager = "AssetManager";
    public static string ViewManager = "ViewManager";
    public static string MsgController = "MsgController";
    public static string LuaScriptMgr = "LuaScriptMgr";
    public static string GameManager = "GameManager";
    public static string VersionManager = "VersionManager";
    public static string DownloadManager = "DownloadManager";    
}
