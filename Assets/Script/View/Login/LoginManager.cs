﻿using UnityEngine;

public class LoginManager : ViewBase
{
    private UIInput mAccountInput;
    private UIInput mPasswordInput;
    private GameObject mLoginBtn;

    public override void Awake()
    {
        base.Awake();
        mAccountInput = transform.Find("AccountInput").GetComponent<UIInput>();
        mPasswordInput = transform.Find("PasswordInput").GetComponent<UIInput>();
        mLoginBtn = transform.Find("LoginBtn").gameObject;
        UIEventListener.Get(mLoginBtn).onClick = OnLogin;
    }
    
    void OnLogin(GameObject sender)
    {
        if (!string.IsNullOrEmpty(mAccountInput.value) && !string.IsNullOrEmpty(mPasswordInput.value))
        {
            _ViewManager.LoadView("prefab/ui/matchview_prefab");
        }
        
        //if (!string.IsNullOrEmpty(mAccountInput.text) && !string.IsNullOrEmpty(mPasswordInput.text))
        //{
        //    LoadingManager.LoadSceneAsync(SceneConfig.Fight);
        //}
    }

    /// <summary>
    /// 处理消息
    /// </summary>
    /// <param name="msg"></param>
    public override void ReceiveMsg(object msg)
    {

    }
}
