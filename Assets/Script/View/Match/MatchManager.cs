﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchManager : ViewBase
{
    private GameObject OneVOneBtn;
    private GameObject TwoVTwoBtn;
    private GameObject ThreeVThreeBtn;

    public override void Awake()
    {
        base.Awake();
        OneVOneBtn = transform.Find("OneVOneBtn").gameObject;
        TwoVTwoBtn = transform.Find("TwoVTwoBtn").gameObject;
        ThreeVThreeBtn = transform.Find("ThreeVThreeBtn").gameObject;
        UIEventListener.Get(OneVOneBtn).onClick = OneVOneBtnFunc;
        UIEventListener.Get(TwoVTwoBtn).onClick = OneVOneBtnFunc;
        UIEventListener.Get(ThreeVThreeBtn).onClick = OneVOneBtnFunc;
    }
    
    void OneVOneBtnFunc(GameObject sender)
    {
        if (sender == OneVOneBtn)
        {
            LoadingManager.LoadSceneAsync(SceneConfig.Fight);
        }
        else if (sender == TwoVTwoBtn)
        {
            LoadingManager.LoadSceneAsync(SceneConfig.Fight);
        }
        else if (sender == ThreeVThreeBtn)
        {
            LoadingManager.LoadSceneAsync(SceneConfig.Fight);
        }
    }

    /// <summary>
    /// 处理消息
    /// </summary>
    /// <param name="msg"></param>
    public override void ReceiveMsg(object msg)
    {

    }
}
