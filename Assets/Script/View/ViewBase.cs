﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ViewAttr))]
public abstract class ViewBase : ScriptBase
{
    public Button mCloseBtn;
    private ViewAttr mViewAttr;

    public virtual void Awake()
    {
        mViewAttr = transform.GetComponent<ViewAttr>();
        _MsgController.Regist(this);
    }

    public virtual void Start()
    {
        mViewAttr.Open();

        if (mCloseBtn != null)
            mCloseBtn.onClick.AddListener(Close);
    }

    public virtual void Close()
    {
        mViewAttr.Close();
    }

    /// <summary>
    /// 加载GameObject，如果加载的GameObject带RendererAttr组件，则要重新计算层次
    /// </summary>
    /// <param name="tPath"></param>
    /// <returns></returns>
    public override GameObject GetGameObject(string tPath, bool tSelfRecord = false)
    {
        GameObject mObj = base.GetGameObject(tPath, tSelfRecord);
        RendererAttr mRendererAttr = mObj.GetComponent<RendererAttr>();
        if (mRendererAttr != null)
            mViewAttr.AddRenderer(mRendererAttr);
        return mObj;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        _MsgController.UnRegist(this);
    }
}
