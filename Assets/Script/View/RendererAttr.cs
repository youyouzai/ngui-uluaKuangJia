﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RendererAttr : MonoBehaviour
{
    //目前只能用于Unity 2d系统(Unity自带的2D，Spine，Uni2D)和粒子系统
    public Renderer[] mRenderer;
}
