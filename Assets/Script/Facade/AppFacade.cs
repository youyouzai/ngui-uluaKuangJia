﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppFacade
{
    private static AppFacade mInstance;
    public static AppFacade Instance
    {
        get
        {
            if (mInstance == null)
            {
                mInstance = new AppFacade();
            }
            return mInstance;
        }
    }

    private GameObject mGameManagerObj;
    private GameObject GameManagerObj
    {
        get
        {
            if (mGameManagerObj == null)
            {
                mGameManagerObj = GameObject.Find("GameManager");
            }
            return mGameManagerObj;
        }
    }

    private Dictionary<string, object> mManagerDic = new Dictionary<string, object>();

    /// <summary>
    /// 启动框架
    /// </summary>
    public void StartUp()
    {
        InitManager();
    }

    void InitManager()
    {
        //-----------------初始化管理器-----------------------
        AddManager(FacadeConfig.LuaScriptMgr, new LuaScriptMgr());

        AddManager<AssetManager>(FacadeConfig.AssetManager);
        AddManager<ViewManager>(FacadeConfig.ViewManager);
        AddManager<MsgController>(FacadeConfig.MsgController);
        AddManager<VersionManager>(FacadeConfig.VersionManager);
        AddManager<DownloadManager>(FacadeConfig.DownloadManager);
        AddManager<GameManager>(FacadeConfig.GameManager);
    }

    public void AddManager(string tName, object tCom)
    {
        mManagerDic[tName] = tCom;
    }

    public void AddManager<T>(string tName) where T : Component
    {
        mManagerDic[tName] = GameManagerObj.AddComponent<T>();
    }

    public T GetManager<T>(string tName) where T : class
    {
        object com;
        if (mManagerDic.TryGetValue(tName, out com))
            return com as T;
        else
            return default(T);
    }

    public void OnDestroy()
    {
        mInstance = null;
    }
}
