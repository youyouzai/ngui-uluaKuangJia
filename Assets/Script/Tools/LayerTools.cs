﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerTools : MonoBehaviour
{
    public Renderer mRenderer;

    public int sortingOrder
    {
        get
        {
            return mRenderer.sortingOrder;
        }
        set
        {
            mRenderer.sortingOrder = value;
        }
    }

#if UNITY_EDITOR
    void Reset()
    {
        mRenderer = GetComponent<Renderer>();
    }
#endif
}
