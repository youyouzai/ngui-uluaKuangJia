﻿using UnityEngine;

public class GameManager : ScriptBase
{
    void Awake()
    {
        Init();
    }

    /// <summary>
    /// 初始化
    /// </summary>
    void Init()
    {
        _LuaScriptMgr.Start();

        DontDestroyOnLoad(gameObject);  //防止销毁自己

        GameObject gui = GetGameObject("prefab/ui/gui_prefab", true);
        DontDestroyOnLoad(gui);

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Application.targetFrameRate = 60;
    }

    void Start()
    {
       _ViewManager.LoadView("prefab/ui/loginview_prefab");
    }

    void Update()
    {
        if (_LuaScriptMgr != null)
        {
            _LuaScriptMgr.Update();
        }
    }

    void LateUpdate()
    {
        if (_LuaScriptMgr != null)
        {
            _LuaScriptMgr.LateUpate();
        }
    }

    void FixedUpdate()
    {
        if (_LuaScriptMgr != null)
        {
            _LuaScriptMgr.FixedUpdate();
        }
    }
}