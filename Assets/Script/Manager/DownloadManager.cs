﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DownloadManager : ScriptBase
{
    public void LoadRes(string tUrl, Action<WWW> tCallBack)
    {
        StartCoroutine(DownLoadEnum(tUrl, null, tCallBack));
    }

    public void LoadRes(string tUrl, Hashtable tHashtable, Action<WWW> tCallBack)
    {
        StartCoroutine(DownLoadEnum(tUrl, tHashtable, tCallBack));
    }

    IEnumerator DownLoadEnum(string tUrl, Hashtable tHashtable, Action<WWW> tCallBack)
    {
        WWW www = null;
        if (tHashtable != null)
        {
            WWWForm form = new WWWForm();
            IDictionaryEnumerator enumrator = tHashtable.GetEnumerator();
            while (enumrator.MoveNext())
            {
                form.AddField(enumrator.Key.ToString(), enumrator.Value.ToString());
            }
            www = new WWW(tUrl, form);
        }
        else
            www = new WWW(tUrl);
        yield return www;
        if (string.IsNullOrEmpty(www.error))
        {
            if (tCallBack != null)
                tCallBack(www);
        }
    }
}
