﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class VersionManager : ScriptBase
{
    public static string mVersion_url = "https://www.baidu.com";

    /// <summary>
    /// 获取当前版本号
    /// </summary>
    /// <returns></returns>
    public string GetOldVersion()
    {
        return "";
    }

    /// <summary>
    /// 获取最新版本号
    /// </summary>
    /// <returns></returns>
    public string GetNewVersion()
    {
        return "";
    }

    /// <summary>
    /// 获取需要更新的资源路径
    /// </summary>
    /// <returns></returns>
    public void GetUpdateResPaths(Action<string[]> tAction)
    {
        StartCoroutine(GetUpdateResPathsEnum(tAction));
    }

    IEnumerator GetUpdateResPathsEnum(Action<string[]> tAction)
    {
        string oldversion = GetOldVersion();
        string newversion = GetNewVersion();
        Hashtable table = new Hashtable() { { "oldversion", oldversion }, { "newversion", newversion } };
        _DownloadManager.LoadRes(mVersion_url, table, (www) =>
          {
              if (string.IsNullOrEmpty(www.error))
              {
                  string[] split = new String[] { "\r\n" };
                  tAction(www.text.Split(split, StringSplitOptions.None));
              }
          });
        yield return null;
    }
}
