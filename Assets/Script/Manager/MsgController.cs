﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MsgController : ScriptBase
{
    private List<ScriptBase> mMsgList = new List<ScriptBase>();

    public void Regist(ScriptBase tScriptBase)
    {
        if (!mMsgList.Contains(tScriptBase))
        {
            mMsgList.Add(tScriptBase);
        }
    }

    public void UnRegist(ScriptBase tScriptBase)
    {
        if (mMsgList.Contains(tScriptBase))
        {
            mMsgList.Remove(tScriptBase);
        }
    }

    public override void ReceiveMsg(object msg)
    {
        for (int i = 0; i < mMsgList.Count; i++)
        {
            mMsgList[i].ReceiveMsg(msg);
        }
    }
}
