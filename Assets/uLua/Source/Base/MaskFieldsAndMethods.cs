﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 生成luaWrap文件后，某些类型的某些字段会导致打包失败，需要添加到该屏蔽列表内
/// </summary>
public class MaskFieldsAndMethods
{
    public static List<string> FieldList = new List<string>()
    {
        
    };
    public static List<string> PropertyList = new List<string>()
    {
        "UnityEngine.MonoBehaviour.runInEditMode",
        "UnityEngine.Light.lightmapBakeType",
    };
    public static List<string> MethodList = new List<string>()
    {

    };
}
