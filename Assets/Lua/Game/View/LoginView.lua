LoginView = {}
LoginView.__index = LoginView
LoginView.Instance = nil

function LoginView.Awake(gameObject)
	print("LoginView.Awake")

    local self = {}
    setmetatable(self, LoginView)
    self.gameObject = gameObject
    self.transform = gameObject.transform
	self.mScriptBase = self.transform:GetComponent("ScriptBase")
	self.mAccountInput = self.transform:Find("AccountInput"):GetComponent("UIInput")
	self.mPasswordInput = self.transform:Find("PasswordInput"):GetComponent("UIInput")
	self.mLoginBtn = self.transform:Find("LoginBtn").gameObject
	
	LoginView.Instance = self
    return self
end

function LoginView:Start()
	print("LoginView.Start")

	self.mScriptBase:AddClick(self.mLoginBtn,function()
		self:OnLogin()
	end)
end

function LoginView:OnLogin()
	print("LoginView.OnLogin")

	if self.mAccountInput.value ~= "" and self.mPasswordInput.value ~= "" then
		self.mScriptBase._ViewManager:LoadView("prefab/ui/matchview_prefab")
	end
end

function LoginView:OnDestroy()
	print("LoginView.OnDestroy")

	LoginView.Instance = nil
end